const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");
const { verify,verifyAdmin } = auth;
console.log(productControllers);

//Get All Active Products
router.get('/activeProducts',/*verify,verifyAdmin,*/productControllers.getAllActiveProducts);

//Create Product (Admin Only)
router.post('/',verify,verifyAdmin,productControllers.createProduct);

//Get Single Product (params)
router.get('/getSingleProduct/:id',productControllers.getSingleProduct);

//Update Product Information (ADMIN ONLY)
router.put('/:id',verify,verifyAdmin,productControllers.updateProductInfo);

//Archive Product (ADMIN ONLY)
router.delete('/archive/:id',verify,verifyAdmin,productControllers.archiveProduct);

////////additional routes////////

//Make Product Available, admin
router.put('/activate/:id',verify,verifyAdmin,productControllers.unarchiveProduct);
//Get All Inactive products, admin
router.get('/inactiveProducts',verify,verifyAdmin,productControllers.getAllInactiveProducts);
module.exports = router;

// find courses by name//update later
// router.post('/findCoursesByName', courseControllers.findCoursesByName);

module.exports = router;