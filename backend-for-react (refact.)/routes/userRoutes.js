const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");
const { verify,verifyAdmin } = auth;
console.log(userControllers);

//User Registration
router.post("/",userControllers.registerUser);

// get all user
router.get('/', userControllers.getAllUsers);

//User Authentication
router.post('/login',userControllers.loginUser);

//Get User Details, (No request body, needs token)
router.get('/getUserDetails',verify,userControllers.getUserDetails);

// check if email exists
router.post('/checkEmailExists',userControllers.checkEmailExists);

// updating user details
router.put('/updateUserDetails', verify, userControllers.updateUserDetails);


//Set User as Admin (ADMIN ONLY)
router.put('/updateAdmin/:id',verify,verifyAdmin,userControllers.makeAdmin);

//Retrieve All Orders (ADMIN ONLY)
router.get('/getAllOrders',verify,verifyAdmin,userControllers.getAllOrders);

// router.post('/createOrder',verify,userControllers.order);


//Retrieve Authenticated Users Orders
router.get('/myOrders',verify,userControllers.getUserOrders);

/////additional routes

//Set Admin - false
router.put('/demoteAdmin/:userId',verify,verifyAdmin,userControllers.demoteAdmin);
module.exports = router;


