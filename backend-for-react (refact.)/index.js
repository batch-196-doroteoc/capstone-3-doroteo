const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://admin:admin123@cluster0.gnqqlit.mongodb.net/ecommerceAPI?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error',console.error.bind(console,"MongoDB Connection Error."));
db.on('open',()=>console.log("Connected to MongoDB."))
app.use(express.json());
const productRoutes = require('./routes/productRoutes');
app.use('/products',productRoutes);
const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);
const orderRoutes = require('./routes/orderRoutes');
app.use('/orders',orderRoutes);
app.listen(port,()=>console.log(`Server is running at port ${port}`));
