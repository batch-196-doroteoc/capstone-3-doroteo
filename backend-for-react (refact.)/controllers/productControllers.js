const Product =require("../models/Product");
//get all active products
module.exports.getAllActiveProducts = (req,res)=>{

	Product.find({isActive:true})
	.then(result=>res.send(result))
	.catch(error=>res.send(error))

};
//add product
module.exports.createProduct = (req,res)=>{
	
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})
	
	newProduct.save()
	.then(result=>res.send(result))
	.catch(error=>res.send(error))
};
//get single product//product id - id
module.exports.getSingleProduct = (req,res)=>{
	// console.log(req.params)
	// console.log(req.params.id)
	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};
//update product
module.exports.updateProductInfo = (req,res) =>{
	// console.log(req.params.id);
	// console.log(req.body);
	let updatedinfo = {
		name:req.body.name,
		description:req.body.description,
		price:req.body.price
	}
	Product.findByIdAndUpdate(req.params.id,updatedinfo,{new:true})
	.then(result=> res.send(result))
	.catch(error=> res.send(error))
};
//archive product
module.exports.archiveProduct = (req,res) => {

	// console.log(req.params.id);
	let update = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.id,update,{new:true})
	.then(result=>res.send(result))
	.catch(error=> res.send(error))
};


////////////////////additional controllers
//activate product
module.exports.unarchiveProduct = (req,res) => {

	// console.log(req.params.id);
	let update = {
		isActive: true
	}

	Product.findByIdAndUpdate(req.params.id,update,{new:true})
	.then(result=>res.send(result))
	.catch(error=> res.send(error))
};

module.exports.getAllInactiveProducts = (req,res)=>{

	Product.find({isActive:false})
	.then(result=>res.send(result))
	.catch(error=>res.send(error))

};

//find products by name

module.exports.findProductsByName = (req, res) => {

	Product.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if(result.length === 0){
			return res.send('No product found!')
		
		} else {

			return res.send(result)
		}
	})
}
