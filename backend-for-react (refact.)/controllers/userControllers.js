const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//REGISTER
//no changes
module.exports.registerUser = (req,res)=>{

	const hashedPw = bcrypt.hashSync(req.body.password,10);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result=>res.send(result))
	.catch(error=>res.send(error))

};
//LOGIN
//refactored: boolean: false
module.exports.loginUser = (req,res)=>{
	console.log(req.body);

	User.findOne({email:req.body.email})
	.then(foundUser=>{
		
		if(foundUser === null){
			return res.send(false)
		}else{
			
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			}else{
				return res.send(false);
			}
		}
	})
};

//MAKE ADMIN
module.exports.makeAdmin = (req,res) => {

	console.log(req.params.id);
	let update = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id,update,{new:true})
	.then(result=>res.send(result))
	.catch(error=> res.send(error))
};

module.exports.getAllOrders = (req,res)=>{

	Order.find({})
	.then(result=>res.send(result))
	.catch(error=>res.send(error))
};

//GET SINGLE USER DETAILS
module.exports.getUserDetails = (req,res)=>{
	console.log(req.user)
	User.findById(req.user.id)
	.then(result=>res.send(result))
	.catch(error=>res.send(error))
};

module.exports.getUserOrders= (req,res)=>{
	if(req.user.isAdmin){
            res.send({message:"Admin is not allowed to retrieve a registered user's order!"})
        }
	Order.find({id:req.user.id})
	.then(result=>res.send(result))
	.catch(error=>res.send(error))
};

/////////additional controllers

module.exports.demoteAdmin = (req,res) => {

	console.log(req.params.id);
	let update = {
		isAdmin: false
	}

	User.findByIdAndUpdate(req.params.id,update,{new:true})
	.then(result=>res.send(result))
	.catch(error=> res.send(error))
};

//RETRIEVE ALL USERS
module.exports.getAllUsers = (req,res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}
//CHECK IF EMAIL EXISTS
module.exports.checkEmailExists = (req,res) => {

	User.findOne({email: req.body.email})
	.then(result => {

		//console.log(result)

		if(result === null){
			return res.send(false);
		} else {
			return res.send(true)
		}

	})
	.catch(err => res.send(err));
}

//update user details

module.exports.updateUserDetails = (req, res) => {
	
	console.log(req.body); //input for new values
	console.log(req.user.id); // check the logged in user's id

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));	
};

