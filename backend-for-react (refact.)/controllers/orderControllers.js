const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.createOrder = (req,res) =>{
        console.log(req.body);
        console.log(req.user.id);

        if(req.user.isAdmin){
            res.send({message:"Admin is not allowed to create an order!"})
        }
        let newOrder = new Order({
            totalAmount: req.body.totalAmount,
            userId: req.user.id,
            products: req.body.products
        });
        
        newOrder.save()
         .then(result=>res.send({message:"Order Successful!"}))
        .catch(error=>res.send(error))

};

module.exports.getAllOrders = (req,res)=>{

    Order.find({})
    .then(result=>res.send(result))
    .catch(error=>res.send(error))
};


module.exports.getUserOrders= (req,res)=>{
    if(req.user.isAdmin){
            res.send({message:"Admin is not allowed to retrieve a registered user's order!"})
        }
    Order.find({id:req.user.id})
    .then(result=>res.send(result))
    .catch(error=>res.send(error))
};

//changed orderId into id
module.exports.getUserProductPerOrder = (req,res)=>{
    // console.log(req.params)
    // console.log(req.params.orderId)
    Order.findById(req.params.id)
    .then(result => res.send(result))
    .catch(error => res.send(error))
};


// module.exports.TotalAmount = (req,res)=>{
//     //access price -- product Schema
//     //access quantity -- order schema

// }